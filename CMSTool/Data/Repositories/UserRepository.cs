﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMSTool.Data.Entities;

namespace CMSTool.Data.Repositories
{
    public interface IUserRepository : IRepository<UserData> { }

    public class UserRepository : Repository<UserData>, IUserRepository
    {
        public UserRepository(ApplicationContext Context) : base(Context) { }
    }
}
